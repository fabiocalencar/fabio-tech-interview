﻿using System.Web.Mvc;

namespace eCommerce.Service.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return new RedirectResult("~/Help");
        }
    }
}
