﻿using Newtonsoft.Json;

namespace eCommerce.Service.Models
{
    public class CartItem
    {
        [JsonProperty("article_id")]
        public int Article_Id { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }
    }
}