﻿using System.IO;

namespace eCommerce.Service.Tests.WebApi
{
    public static class ContentSource
    {
        public static string GetContent(SourceType type, SourceLevel level)
        {
            string path = Path.Combine(
                Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName,
                string.Format("WebApi\\{0}\\{1}.json", level.ToString(), type.ToString()));

            return File.ReadAllText(path);
        }
    }

    public enum SourceType
    {
        Data,
        Output
    }

    public enum SourceLevel
    {
        Level1,
        Level2,
        Level3
    }
}
