﻿using Newtonsoft.Json;

namespace eCommerce.Service.Models
{
    public class Discount
    {
        [JsonProperty("article_id")]
        public int Article_Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public double Value { get; set; }
    }
}