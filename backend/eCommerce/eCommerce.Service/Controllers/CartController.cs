﻿using eCommerce.Service.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace eCommerce.Service.Controllers
{
    public class CartController : ApiController
    {
        // Level 1
        // POST: /Cart/AddOrder
        [System.Web.Http.HttpPost]
        [Route("api/cart/addorder/{order}")]
        public PurchaseTotal AddOrder([FromBody] PurchaseOrder order)
        {
            // Check if have purchase order
            if (order == null)
                return null;

            // Check if have articles
            if (order.Articles == null)
                return null;
            
            // Check for items in the carts
            if (order.Carts == null)
                return null;

            var purchaseTotal = new PurchaseTotal()
            {
                Carts = new List<CartTotal>()
            };

            foreach (Cart cart in order.Carts)
            {
                double total = 0;

                // Verify that the item was found
                // Calculate total carts prices
                cart.Items.ToList().ForEach(i => total += order.Articles.Any(a => a.Id == i.Article_Id) ?
                            order.Articles.Where(a => a.Id == i.Article_Id).First().Price * i.Quantity : 0);

                var cartTotal = new CartTotal()
                {
                    Id = cart.Id,
                    Total = total
                };

                purchaseTotal.Carts.Add(cartTotal);
            }

            // return result
            return purchaseTotal;
        }

        // Level 2
        // POST: /Cart/CalculateShipping
        [System.Web.Http.HttpPost]
        [Route("api/cart/calculateshipping/{order}")]
        public PurchaseTotal CalculateShipping([FromBody] PurchaseOrder order)
        {
            // Check if have purchase order
            if (order == null)
                return null;
            
            var purchaseTotal = AddOrder(order);

            // Check if purchase is null
            if (purchaseTotal == null)
                return null;

            // Check if have delivery fees
            if (order.Delivery_Fees == null)
                return null;

            // Calculate Shipping
            purchaseTotal.Carts.ToList()
                .ForEach(c => c.Total += GetShipping(order, c.Total));

            // return result
            return purchaseTotal;
        }

        // Level 3
        // POST: /Cart/CalculateDiscount
        [System.Web.Http.HttpPost]
        [Route("api/cart/calculatediscount/{order}")]
        public PurchaseTotal CalculateDiscount([FromBody] PurchaseOrder order)
        {
            // Check if have purchase order
            if (order == null)
                return null;

            var purchaseTotal = AddOrder(order);

            // Check if purchase is null
            if (purchaseTotal == null)
                return null;

            // Check if have delivery fees
            if (order.Delivery_Fees == null)
                return null;

            // Calculate Discount
            purchaseTotal.Carts.ToList()
                .ForEach(ct => order.Carts.Where(c => c.Id == ct.Id).ToList()
                    .ForEach(c => c.Items.ToList()
                        .ForEach(i => ct.Total -= GetDiscount(order, i.Article_Id) * i.Quantity)));

            // Calculate Shipping
            purchaseTotal.Carts.ToList()
                .ForEach(c => c.Total += GetShipping(order, c.Total));

            // return result
            return purchaseTotal;
        }

        private double GetShipping(PurchaseOrder order, double value)
        {
            return order.Delivery_Fees.Where(f => f.Eligible_Transaction_Volume.Min_Price <= value &&
                                (f.Eligible_Transaction_Volume.Max_Price.HasValue ?
                                f.Eligible_Transaction_Volume.Max_Price.Value :
                                double.MaxValue) > value)
                       .Select(f => f.Price)
                       .Single();
        }

        private double GetDiscount(PurchaseOrder order, int article_id)
        {
            double valueDiscount = 0;

            // Check if discounts is null
            if (order.Discounts == null)
                return valueDiscount;

            // Check if exits discount for article
            if (!order.Discounts.Any(d => d.Article_Id == article_id))
                return valueDiscount;

            Discount discount = order.Discounts.Where(d => d.Article_Id == article_id).Single();
            Article article = order.Articles.Where(a => a.Id == article_id).Single();

            if (discount.Type == "amount") // Calculate if amount
            {
                valueDiscount = discount.Value;
            }
            else if (discount.Type == "percentage") // Calculate if percentage
            {
                valueDiscount = article.Price * (discount.Value / 100);
            }

            return valueDiscount;
        }
    }
}
