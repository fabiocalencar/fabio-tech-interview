﻿using Newtonsoft.Json;

namespace eCommerce.Service.Models
{
    public class DeliveryFee
    {
        [JsonProperty("eligible_transaction_volume")]
        public TransactionVolume Eligible_Transaction_Volume { get; set; }

        [JsonProperty("price")]
        public double Price { get; set; }
    }
}