﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace eCommerce.Service.Models
{
    public class PurchaseTotal
    {
        [JsonProperty("carts")]
        public IList<CartTotal> Carts { get; set; }
    }
}