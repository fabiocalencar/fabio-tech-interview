﻿using eCommerce.Service.Controllers;
using eCommerce.Service.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace eCommerce.Service.Tests.Controllers
{
    [TestClass]
    public class CartControllerTest
    {
        [TestMethod]
        public void AddOrderWithoutPurchaseOrder()
        {
            var controller = new CartController();
            var total = controller.AddOrder(null);

            // Check value
            Assert.IsNull(total);
        }
        
        [TestMethod]
        public void AddOrderWithoutCartItem()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "test", Price = 100 }
                }
            };

            var controller = new CartController();
            var total = controller.AddOrder(order);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void AddOrderWithoutArticle()
        {
            var order = new PurchaseOrder()
            {
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 1, Quantity = 10 }
                        }
                    }
                }
            };

            var controller = new CartController();
            var total = controller.AddOrder(order);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void AddOrderArticleNotFound()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "water", Price = 100 },
                    new Article() { Id = 2, Name = "honey", Price = 200 },
                    new Article() { Id = 3, Name = "mango", Price = 400 },
                    new Article() { Id = 4, Name = "tea", Price = 1000 }
                },
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 5, Quantity = 2 }
                        }
                    }                  
                }
            };

            var controller = new CartController();
            var total = controller.AddOrder(order);

            // Check values
            Assert.IsNotNull(total);
            Assert.IsNotNull(total.Carts);
            Assert.AreNotEqual(0, total.Carts);
            Assert.AreEqual(0, total.Carts.Where(c => c.Id == 1).First().Total);
        }
        
        [TestMethod]
        public void AddOrderTest()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "water", Price = 100 },
                    new Article() { Id = 2, Name = "honey", Price = 200 },
                    new Article() { Id = 3, Name = "mango", Price = 400 },
                    new Article() { Id = 4, Name = "tea", Price = 1000 }
                },
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 1, Quantity = 6 },
                            new CartItem() { Article_Id = 2, Quantity = 2 },
                            new CartItem() { Article_Id = 4, Quantity = 1 }
                        }
                    },
                    new Cart()
                    {
                        Id = 2,
                        Items = new List<CartItem>()
                        {
                            new CartItem() { Article_Id = 2, Quantity = 1 },
                            new CartItem() { Article_Id = 3, Quantity = 3 }
                        }
                    },
                    new Cart()
                    {
                        Id = 3,
                        Items = new List<CartItem>()
                    }                   
                }
            };

            var controller = new CartController();
            var total = controller.AddOrder(order);

            // Check values
            Assert.IsNotNull(total);
            Assert.IsNotNull(total.Carts);
            Assert.AreNotEqual(0, total.Carts);
            Assert.AreEqual(2000, total.Carts.Where(c => c.Id == 1).First().Total);
            Assert.AreEqual(1400, total.Carts.Where(c => c.Id == 2).First().Total);
            Assert.AreEqual(0, total.Carts.Where(c => c.Id == 3).First().Total);
        }

        [TestMethod]
        public void CalculateShippingWithoutPurchaseOrder()
        {
            var controller = new CartController();
            var total = controller.CalculateShipping(null);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void CalculateShippingWithoutCartItem()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "test", Price = 100 }
                }
            };

            var controller = new CartController();
            var total = controller.CalculateShipping(order);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void CalculateShippingWithoutArticle()
        {
            var order = new PurchaseOrder()
            {
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 1, Quantity = 10 }
                        }
                    }
                }
            };

            var controller = new CartController();
            var total = controller.CalculateShipping(order);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void CalculateShippingWithoutDeliveryFee()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "water", Price = 100 },
                    new Article() { Id = 2, Name = "honey", Price = 200 },
                    new Article() { Id = 3, Name = "mango", Price = 400 },
                    new Article() { Id = 4, Name = "tea", Price = 1000 }
                },
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 2, Quantity = 1 },
                            new CartItem() { Article_Id = 3, Quantity = 5 }
                        }
                    }                  
                }
            };

            var controller = new CartController();
            var total = controller.CalculateShipping(order);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void CalculateShippingArticleNotFound()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "water", Price = 100 },
                    new Article() { Id = 2, Name = "honey", Price = 200 },
                    new Article() { Id = 3, Name = "mango", Price = 400 },
                    new Article() { Id = 4, Name = "tea", Price = 1000 }
                },
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 5, Quantity = 2 }
                        }
                    }                  
                },
                Delivery_Fees = new List<DeliveryFee>() 
                { 
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 0,
                            Max_Price = 1000 
                        },
                        Price = 800
                    },
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 1000,
                            Max_Price = 2000 
                        },
                        Price = 400
                    },
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 2000,
                            Max_Price = null 
                        },
                        Price = 0
                    }
                }
            };

            var controller = new CartController();
            var total = controller.CalculateShipping(order);

            // Check values
            Assert.IsNotNull(total);
            Assert.IsNotNull(total.Carts);
            Assert.AreNotEqual(0, total.Carts);
            Assert.AreEqual(800, total.Carts.Where(c => c.Id == 1).First().Total);
        }

        [TestMethod]
        public void CalculateShippingTest()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "water", Price = 100 },
                    new Article() { Id = 2, Name = "honey", Price = 200 },
                    new Article() { Id = 3, Name = "mango", Price = 400 },
                    new Article() { Id = 4, Name = "tea", Price = 1000 }
                },
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 1, Quantity = 6 },
                            new CartItem() { Article_Id = 2, Quantity = 2 },
                            new CartItem() { Article_Id = 4, Quantity = 1 }
                        }
                    },
                    new Cart()
                    {
                        Id = 2,
                        Items = new List<CartItem>()
                        {
                            new CartItem() { Article_Id = 2, Quantity = 1 },
                            new CartItem() { Article_Id = 3, Quantity = 3 }
                        }
                    },
                    new Cart()
                    {
                        Id = 3,
                        Items = new List<CartItem>()
                    }                   
                },
                Delivery_Fees = new List<DeliveryFee>() 
                { 
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 0,
                            Max_Price = 1000 
                        },
                        Price = 800
                    },
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 1000,
                            Max_Price = 2000 
                        },
                        Price = 400
                    },
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 2000,
                            Max_Price = null 
                        },
                        Price = 0
                    }
                }
            };

            var controller = new CartController();
            var total = controller.CalculateShipping(order);

            // Check values
            Assert.IsNotNull(total);
            Assert.IsNotNull(total.Carts);
            Assert.AreNotEqual(0, total.Carts);
            Assert.AreEqual(2000, total.Carts.Where(c => c.Id == 1).First().Total);
            Assert.AreEqual(1800, total.Carts.Where(c => c.Id == 2).First().Total);
            Assert.AreEqual(800, total.Carts.Where(c => c.Id == 3).First().Total);
        }

        [TestMethod]
        public void CalculateDiscountWithoutPurchaseOrder()
        {
            var controller = new CartController();
            var total = controller.CalculateDiscount(null);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void CalculateDiscountWithoutCartItem()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "test", Price = 100 }
                }
            };

            var controller = new CartController();
            var total = controller.CalculateDiscount(order);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void CalculateDiscountWithoutArticle()
        {
            var order = new PurchaseOrder()
            {
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 1, Quantity = 10 }
                        }
                    }
                }
            };

            var controller = new CartController();
            var total = controller.CalculateDiscount(order);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void CalculateDiscountWithoutDeliveryFee()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "water", Price = 100 },
                    new Article() { Id = 2, Name = "honey", Price = 200 },
                    new Article() { Id = 3, Name = "mango", Price = 400 },
                    new Article() { Id = 4, Name = "tea", Price = 1000 }
                },
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 2, Quantity = 1 },
                            new CartItem() { Article_Id = 3, Quantity = 5 }
                        }
                    }                  
                }
            };

            var controller = new CartController();
            var total = controller.CalculateDiscount(order);

            // Check value
            Assert.IsNull(total);
        }

        [TestMethod]
        public void CalculateDiscountArticleNotFound()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "water", Price = 100 },
                    new Article() { Id = 2, Name = "honey", Price = 200 },
                    new Article() { Id = 3, Name = "mango", Price = 400 },
                    new Article() { Id = 4, Name = "tea", Price = 1000 }
                },
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 5, Quantity = 2 }
                        }
                    }                  
                },
                Delivery_Fees = new List<DeliveryFee>() 
                { 
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 0,
                            Max_Price = 1000 
                        },
                        Price = 800
                    },
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 1000,
                            Max_Price = 2000 
                        },
                        Price = 400
                    },
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 2000,
                            Max_Price = null 
                        },
                        Price = 0
                    }
                }
            };

            var controller = new CartController();
            var total = controller.CalculateDiscount(order);

            // Check values
            Assert.IsNotNull(total);
            Assert.IsNotNull(total.Carts);
            Assert.AreNotEqual(0, total.Carts);
            Assert.AreEqual(800, total.Carts.Where(c => c.Id == 1).First().Total);
        }

        [TestMethod]
        public void CalculateDiscountTest()
        {
            var order = new PurchaseOrder()
            {
                Articles = new List<Article>() 
                { 
                    new Article() { Id = 1, Name = "water", Price = 100 },
                    new Article() { Id = 2, Name = "honey", Price = 200 },
                    new Article() { Id = 3, Name = "mango", Price = 400 },
                    new Article() { Id = 4, Name = "tea", Price = 1000 },
                    new Article() { Id = 5, Name = "ketchup", Price = 999 },
                    new Article() { Id = 6, Name = "mayonnaise", Price = 999 },
                    new Article() { Id = 7, Name = "fries", Price = 378 },
                    new Article() { Id = 8, Name = "ham", Price = 147 }
                },
                Carts = new List<Cart>() 
                { 
                    new Cart() 
                    { 
                        Id = 1,
                        Items = new List<CartItem>() 
                        {
                            new CartItem() { Article_Id = 1, Quantity = 6 },
                            new CartItem() { Article_Id = 2, Quantity = 2 },
                            new CartItem() { Article_Id = 4, Quantity = 1 }
                        }
                    },
                    new Cart()
                    {
                        Id = 2,
                        Items = new List<CartItem>()
                        {
                            new CartItem() { Article_Id = 2, Quantity = 1 },
                            new CartItem() { Article_Id = 3, Quantity = 3 }
                        }
                    },
                    new Cart()
                    {
                        Id = 3,
                        Items = new List<CartItem>()
                        {
                            new CartItem() { Article_Id = 5, Quantity = 1 },
                            new CartItem() { Article_Id = 6, Quantity = 1 }
                        }
                    },
                    new Cart()
                    {
                        Id = 4,
                        Items = new List<CartItem>()
                        {
                            new CartItem() { Article_Id = 7, Quantity = 1 }    
                        }
                    },
                    new Cart()
                    {
                        Id = 5,
                        Items = new List<CartItem>()
                        {
                            new CartItem() { Article_Id = 8, Quantity = 3 }    
                        }
                    }
                },
                Delivery_Fees = new List<DeliveryFee>() 
                { 
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 0,
                            Max_Price = 1000 
                        },
                        Price = 800
                    },
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 1000,
                            Max_Price = 2000 
                        },
                        Price = 400
                    },
                    new DeliveryFee()
                    {
                        Eligible_Transaction_Volume = new TransactionVolume() 
                        { 
                            Min_Price = 2000,
                            Max_Price = null 
                        },
                        Price = 0
                    }
                },
                Discounts = new List<Discount>() 
                { 
                    new Discount() 
                    { 
                        Article_Id = 2,
                        Type = "amount",
                        Value = 25
                    },
                    new Discount() 
                    { 
                        Article_Id = 5,
                        Type = "percentage",
                        Value = 30
                    },
                    new Discount() 
                    { 
                        Article_Id = 6,
                        Type = "percentage",
                        Value = 30
                    },
                    new Discount() 
                    { 
                        Article_Id = 7,
                        Type = "percentage",
                        Value = 25
                    },
                    new Discount() 
                    { 
                        Article_Id = 8,
                        Type = "percentage",
                        Value = 10
                    }
                }
            };

            var controller = new CartController();
            var total = controller.CalculateDiscount(order);

            // Check values
            Assert.IsNotNull(total);
            Assert.IsNotNull(total.Carts);
            Assert.AreNotEqual(0, total.Carts);
            Assert.AreEqual(2350, total.Carts.Where(c => c.Id == 1).First().Total);
            Assert.AreEqual(1775, total.Carts.Where(c => c.Id == 2).First().Total);
            Assert.AreEqual(1798.6d, total.Carts.Where(c => c.Id == 3).First().Total);
            Assert.AreEqual(1083.5d, total.Carts.Where(c => c.Id == 4).First().Total);
            Assert.AreEqual(1196.9d, total.Carts.Where(c => c.Id == 5).First().Total);
        }
    }
}
