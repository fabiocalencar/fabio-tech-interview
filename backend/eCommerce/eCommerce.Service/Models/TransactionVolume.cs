﻿using Newtonsoft.Json;

namespace eCommerce.Service.Models
{
    public class TransactionVolume
    {
        [JsonProperty("min_price")]
        public double Min_Price { get; set; }

        [JsonProperty("max_price")]
        public double? Max_Price { get; set; } 
    }
}