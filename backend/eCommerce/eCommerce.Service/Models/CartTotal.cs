﻿using Newtonsoft.Json;

namespace eCommerce.Service.Models
{
    public class CartTotal
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("total")]
        public double Total { get; set; }
    }
}