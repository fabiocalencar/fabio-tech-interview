﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace eCommerce.Service.Models
{
    public class Cart
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("items")]
        public IList<CartItem> Items { get; set; }
    }
}