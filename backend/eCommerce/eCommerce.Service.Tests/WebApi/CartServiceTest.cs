﻿using eCommerce.Service.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web.Http;

namespace eCommerce.Service.Tests.WebApi
{
    [TestClass]
    public class CartServiceTest
    {
        private Process process;
        private HttpConfiguration configuration;

        private void StartProcess()
        {
            var processStartInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Normal,
                ErrorDialog = true,
                LoadUserProfile = true,
                CreateNoWindow = false,
                UseShellExecute = false,
                Arguments = string.Format("/path:\"{0}\" /port:{1}", "http://localhost", 32008)
            };

            var programFile = string.IsNullOrEmpty(processStartInfo.EnvironmentVariables["programfiles"]) ?
                processStartInfo.EnvironmentVariables["programfiles(x86)"] :
                processStartInfo.EnvironmentVariables["programfiles"];

            processStartInfo.FileName = programFile + "\\IIS Express\\iisexpress.exe";

            try
            {
                process = new Process { StartInfo = processStartInfo };

                process.Start();
                process.WaitForExit();
            }
            catch
            {
                process.CloseMainWindow();
                process.Dispose();
            }
        }

        [TestInitialize]
        public void Setup()
        {
            // Run IIS Express for testing
            var thread = new Thread(StartProcess) { IsBackground = true };
            thread.Start();

            configuration = new HttpConfiguration();
            WebApiConfig.Register(configuration);
        }

        [TestCleanup]
        public void Close()
        {
            process.Dispose();
        }
        
        [TestMethod]
        public void PostAddOrder()
        {
            // Get content from files
            var data = ContentSource.GetContent(SourceType.Data, SourceLevel.Level1);
            var output = ContentSource.GetContent(SourceType.Output, SourceLevel.Level1);

            // Json request
            var content = new StringContent(data, Encoding.UTF8, "application/json");

            using (var server = new HttpServer(configuration))
            using (var client = new HttpClient(server))
            using (var response = client.PostAsync(new Uri("http://localhost:32008/api/cart/addorder"), content).Result)
            {   
                // Check response of request
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                // Json response
                string outputContent = response.Content.ReadAsStringAsync().Result;

                // Deserialize expected and actual object for testing
                PurchaseTotal totalExpected = JsonConvert.DeserializeObject<PurchaseTotal>(output);
                PurchaseTotal totalActual = JsonConvert.DeserializeObject<PurchaseTotal>(outputContent);

                // Check values
                Assert.IsNotNull(totalActual.Carts);
                Assert.AreEqual(totalExpected.Carts.Count, totalActual.Carts.Count);
                Assert.AreEqual(totalExpected.Carts[0].Id, totalActual.Carts[0].Id);
                Assert.AreEqual(totalExpected.Carts[0].Total, totalActual.Carts[0].Total);
                Assert.AreEqual(totalExpected.Carts[1].Id, totalActual.Carts[1].Id);
                Assert.AreEqual(totalExpected.Carts[1].Total, totalActual.Carts[1].Total);
                Assert.AreEqual(totalExpected.Carts[2].Id, totalActual.Carts[2].Id);
                Assert.AreEqual(totalExpected.Carts[2].Total, totalActual.Carts[2].Total);
            };
        }

        [TestMethod]
        public void PostCalculateShipping()
        {
            // Get content from files
            var data = ContentSource.GetContent(SourceType.Data, SourceLevel.Level2);
            var output = ContentSource.GetContent(SourceType.Output, SourceLevel.Level2);

            // Json request
            var content = new StringContent(data, Encoding.UTF8, "application/json");

            using (var server = new HttpServer(configuration))
            using (var client = new HttpClient(server))
            using (var response = client.PostAsync(new Uri("http://localhost:32008/api/cart/calculateshipping"), content).Result)
            {
                // Check response of request
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                // Json response
                string outputContent = response.Content.ReadAsStringAsync().Result;

                // Deserialize expected and actual object for testing
                PurchaseTotal totalExpected = JsonConvert.DeserializeObject<PurchaseTotal>(output);
                PurchaseTotal totalActual = JsonConvert.DeserializeObject<PurchaseTotal>(outputContent);

                // Check values
                Assert.IsNotNull(totalActual.Carts);
                Assert.AreEqual(totalExpected.Carts.Count, totalActual.Carts.Count);
                Assert.AreEqual(totalExpected.Carts[0].Id, totalActual.Carts[0].Id);
                Assert.AreEqual(totalExpected.Carts[0].Total, totalActual.Carts[0].Total);
                Assert.AreEqual(totalExpected.Carts[1].Id, totalActual.Carts[1].Id);
                Assert.AreEqual(totalExpected.Carts[1].Total, totalActual.Carts[1].Total);
                Assert.AreEqual(totalExpected.Carts[2].Id, totalActual.Carts[2].Id);
                Assert.AreEqual(totalExpected.Carts[2].Total, totalActual.Carts[2].Total);
            };
        }

        [TestMethod]
        public void PostCalculateDiscount()
        {
            // Get content from files
            var data = ContentSource.GetContent(SourceType.Data, SourceLevel.Level3);
            var output = ContentSource.GetContent(SourceType.Output, SourceLevel.Level3);

            // Json request
            var content = new StringContent(data, Encoding.UTF8, "application/json");

            using (var server = new HttpServer(configuration))
            using (var client = new HttpClient(server))
            using (var response = client.PostAsync(new Uri("http://localhost:32008/api/cart/calculatediscount"), content).Result)
            {
                // Check response of request
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                // Json response
                string outputContent = response.Content.ReadAsStringAsync().Result;

                // Deserialize expected and actual object for testing
                PurchaseTotal totalExpected = JsonConvert.DeserializeObject<PurchaseTotal>(output);
                PurchaseTotal totalActual = JsonConvert.DeserializeObject<PurchaseTotal>(outputContent);

                // Check values
                Assert.IsNotNull(totalActual.Carts);
                Assert.AreEqual(totalExpected.Carts.Count, totalActual.Carts.Count);
                Assert.AreEqual(totalExpected.Carts[0].Id, totalActual.Carts[0].Id);
                Assert.AreEqual(totalExpected.Carts[0].Total, totalActual.Carts[0].Total);
                Assert.AreEqual(totalExpected.Carts[1].Id, totalActual.Carts[1].Id);
                Assert.AreEqual(totalExpected.Carts[1].Total, totalActual.Carts[1].Total);
                Assert.AreEqual(totalExpected.Carts[2].Id, totalActual.Carts[2].Id);
                Assert.AreEqual(totalExpected.Carts[2].Total, totalActual.Carts[2].Total);
                Assert.AreEqual(totalExpected.Carts[3].Id, totalActual.Carts[3].Id);
                Assert.AreEqual(totalExpected.Carts[3].Total, totalActual.Carts[3].Total);
                Assert.AreEqual(totalExpected.Carts[4].Id, totalActual.Carts[4].Id);
                Assert.AreEqual(totalExpected.Carts[4].Total, totalActual.Carts[4].Total);
            };
        }
    }
}
