﻿using Newtonsoft.Json;

namespace eCommerce.Service.Models
{
    public class Article
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("price")]
        public double Price { get; set; }
    }
}