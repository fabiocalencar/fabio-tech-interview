﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace eCommerce.Service.Models
{
    public class PurchaseOrder
    {
        [JsonProperty("articles")]
        public IList<Article> Articles { get; set; }

        [JsonProperty("carts")]
        public IList<Cart> Carts { get; set; }

        [JsonProperty("delivery_fees")]
        public IList<DeliveryFee> Delivery_Fees { get; set; }

        [JsonProperty("discounts")]
        public IList<Discount> Discounts { get; set; }
    }
}